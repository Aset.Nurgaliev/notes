/*let num = 38; //number
let firstName = 'Aset'; //string
const isProgrammer = true; //boolean
*/
/*Can Do
let $ = 'test';
let $num = 38;
let num$ = 38;
let _ = 38;
let _num = 38;
let num_ = 38;
let first_name = 'Aset'; // так можно, но не желательно
let myNum = 38;
let num38 = 38;
*/

/* Restricted (запрещенные названия переменных)
let 38num = '15';
let my-num = 1;
let let = 'bob';
let const = 5;
*/
//firstName = 'Sabr'; // no error
//isProgrammer = false; // error because of const

//alert (firstName)
//console.log('test:' ,firstName, num)

/*console.log(num + 10)
console.log(num - 10)
console.log(num * 10)
console.log(num / 10)
console.log(num)
*/
/*let num2 = num + 10
console.log(num, num2)
num = num2 - num
console.log(num, num2)

let num3 = (num + 10 * 2) / 5 - 1
console.log(num3)
*/
//const fullName = firstName + ' Nurgali'
//const fullName = firstName +  '\n' + 'Nurgali'
//console.log(fullName)

const resultElement = document.getElementById('result')
const input1 = document.getElementById('input1')
const input2 = document.getElementById('input2')
const submitBtn = document.getElementById('submit')
const plusBtn = document.getElementById('plus')
const minusBtn = document.getElementById('minus')
const razdelitBtn = document.getElementById('razdelit')
const umnojitBtn = document.getElementById('umnojit')
//console.log(resultElement.textContent)
//resultElement.textContent = 38


//console.log( typeof sum)

plusBtn.onclick = function() {
    action = '+'
}
minusBtn.onclick = function() {
    action = '-'
}
razdelitBtn.onclick = function() {
    action = '/'
}
umnojitBtn.onclick = function() {
    action = '*'
}

function printResult(result) {
    if (result < 0 ) {
        resultElement.style.color = 'red'
    } else {
        resultElement.style.color = 'green'
    }
    resultElement.textContent = result
}

function computeNumbersWithAction( inp1, inp2, actionSymbol) {
    const num1 = Number(inp1.value)
    const num2 = Number(inp2.value)
    if(actionSymbol == '+') {
        return num1 + num2
    }
    if(actionSymbol =='-') {
        return num1 - num2
    }
    if(actionSymbol == '/') {
        return num1 / num2
    }
    if(actionSymbol == '*') {
        return num1 * num2
    }
} 

submitBtn.onclick = function () {
    const result = computeNumbersWithAction(input1, input2, action)
    printResult(result)
    //if (action == '+') {
    //    const sum = Number(input1.value) + Number(input2.value)
    //    printResult (sum)
    //} else if (action == '-') {
    //    const sum = Number(input1.value) - Number(input2.value)
    //    printResult (sum)
    //}
    
}

