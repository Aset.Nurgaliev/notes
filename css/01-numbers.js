//const num = 38 //integer
//const float = 38.38 //float
//const pow = 10e3 
//const big = 1_000_000_000
//const negative = -10

//console.log(Number.MAX_SAFE_INTEGER)
//console.log(Math.pow(2, 53) - 1)
//console.log(Number.MIN_SAFE_INTEGER)
//console.log(Number.MAX_VALUE)
//console.log(Number.MIN_VALUE)
//console.log(Number.POSITIVE_INFINITY)
//console.log(Number.NEGATIVE_INFINITY)
//console.log(1 / 0)
//console.log(Number.isFinite(1 / 0))
//console.log(23 / undefined)

//const weird = 23 / undefined
//console.log(Number.isNaN(weird))
//console.log(Number.isNaN(38)) 

//const strInt = '38'
//const floatInt = '38.38'

//console.log(Number(strInt))
//console.log(Number(floatInt))
//console.log(Number.parseInt(strInt))
//console.log(Number.parseFloat(floatInt))
//console.log(+strInt, +floatInt)

//console.log(+(0.1 + 0.2).toFixed(1))
//console.log(parseFloat((0.1 + 0.2).toFixed(1)))
//const fixed = (0.1 + 0.2).toFixed(1)
//console.log(parseFloat(fixed))

//BigInt (целочисленный тип)
//console.log(BigInt(Number.MAX_SAFE_INTEGER) + 5428963n)
//console.log(typeof-42n)
//console.log(10n - 4) // error
//console.log(parseInt(10n) - 4)
//console.log(10n - BigInt(4))
//console.log(5n / 2n)
//console.log(5 / 2)

//Math

//const myNum = 4.9
//console.log(Math.E)
//console.log(Math.PI)
//console.log(Math.sqrt(25)) //квадратный корень
//console.log(Math.pow(2, 3)) //возведение в степень
//console.log(Math.abs(-38)) //абсолютное значение (модуль)
//console.log(Math.max(2,9,5,200,35,84,0)) //возвращает максимальное число
//console.log(Math.min(2,9,5,200,35,84,0)) //возвращает минимальное число
//console.log(Math.floor(myNum)) //округление в меньшую сторону
//console.log(Math.ceil(myNum)) //округление в большую сторону
//console.log(Math.round(myNum)) //округление до ближайшего целочисленного
//console.log(Math.trunc(myNum)) //возвращает только целую часть
//console.log(Math.random()) //всегда возвращает случайное значение в диапазоне от 0 до 1

//function getRandomNumber (min, max) {
//    return Math.floor(Math.random() * (max - min + 1) + min)
//}
//const num1 = getRandomNumber(10, 100)
//console.log(num1)