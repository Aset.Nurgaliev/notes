const firstName = 'Aset'
const age = 38
//const old = 'Hello my name is ' + name + ' and my age is ' + 38 //старый вариант
//function getAge() {
//    return age
//}
//const output = `Hello my name is ${name} and my age is ${
//    getAge() > 18 ? 'Can drive' : `Can't drive`
//}` //в обратных кавычках можем переносить по строкам

//console.log(output)

console.log(firstName.length)
console.log(firstName)
console.log(firstName.toUpperCase())
console.log(firstName.toLowerCase())
console.log(firstName.charAt(3))
console.log(firstName.indexOf('A'))
console.log(firstName.startsWith('s'))
console.log(firstName.toLocaleLowerCase().startsWith('a'))
console.log(firstName.endsWith('et'))
console.log(firstName.repeat(5))

const password = '   my super pass   '
console.log(password.trim()) //этот метод убирает лишние пробелы


