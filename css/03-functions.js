//Есть два способа объявления фуцнкции

//=====Function Declaration=====

//function greet(name) {
//    console.log('Salem -', name)
//}
//greet('alem')

//=====Function Expression=====

//const greet2 = function (name) {
//    console.log('Salam ',name)
//}
//greet2('alem')

//setTimeout(function() {
//    greet('alem')
//}, 1500) //базовая функция в JS которая вызывает функционал по истечении времени

//let counter = 0
//const interval = setInterval(function() {
//    if(counter === 5) {
//        clearInterval(interval)
//    } else {
//        console.log(++counter)
//    }
//console.log(++counter)
//

//=====Arrow Function=====

function greet(name) {
    console.log('Salem -', name)
}

const arrow = (name, age) => {
    console.log('Salem -', name, age)
}

const arrow2 = (name) => console.log('Salem -', name)

function pow(num, exp) {
    return Math.pow(num,exp)
}

const pow2 = (num, exp) => Math.pow(num, exp)
//console.log(pow2(2, 5))

//arrow2('alem', 55)

//=====Default Parameters=====

const sum = (a, b) => a + b
//console.log(sum(55, 38))

function sumAll(...numbers) {
//    let res = 0
//    for (let num of numbers) {
 //       res += num
 //   }
 //   return res
 return numbers.reduce((acc, cur) => (acc += cur), 0)
}
console.log(sumAll(1,2,3,4,5))

//=====Closures===== замыкания

function createPerson (name) {
    return function (lastName){
        console.log(name + ' ' + lastName)
    }
}
const addLastName = createPerson('Aset')
addLastName('Nurgiliyev')
addLastName('Nurgili')